import rasterio
from rasterio import features
import geopandas as gpd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
import tensorflow as tf
from tensorflow import keras
from skimage import exposure, io, transform

# Load the satellite image
with rasterio.open('task_04/T36UXV_20200406T083559_TCI_10m.jp2') as dataset:
    image_data = dataset.read()
    image_metadata = dataset.meta

# EDA: Get basic information about the image data
print('Image shape:', image_data.shape)
print('Number of bands:', image_data.shape[0])
print('Data type:', image_data.dtype)
print('Minimum pixel value:', image_data.min())
print('Maximum pixel value:', image_data.max())

# # EDA: Visualize the image bands
# import matplotlib.pyplot as plt
# fig, axes = plt.subplots(nrows=3, ncols=3, figsize=(15,15))
# for i, ax in enumerate(axes.flatten()):
#     ax.imshow(image_data[i], cmap='gray')
#     ax.set_title(f'Band {i+1}')
# plt.tight_layout()

# # Preprocess the image
# # Reshape the image to be a 2D array (height x width, num_bands)
# image_2d = image_data.transpose(1, 2, 0).reshape(-1, image_data.shape[0])

# # Scale the pixel values using the StandardScaler
# scaler = StandardScaler()
# image_scaled = scaler.fit_transform(image_2d)

# # Reshape the scaled image back to its original shape
# image_processed = image_scaled.reshape(image_data.shape[1], image_data.shape[2], -1)

# # Load the erosion mask
# erosion_mask = gpd.read_file('task_04/masks/Masks_T36UXV_20190427.shp')

# # Rasterize the erosion mask
# erosion_mask = rasterio.features.geometry_mask(
#     erosion_mask.geometry,
#     out_shape=image_data.shape[-2:],
#     transform=image_metadata['transform'],
#     invert=True
# )

# # Create the label mask by combining the erosion and non-erosion masks
# label_mask = np.zeros_like(erosion_mask, dtype=np.uint8)
# label_mask[erosion_mask == 1] = 1

# Preprocess the image
# Rescale the pixel values to a range of 0-1
image_rescaled = exposure.rescale_intensity(image_data, out_range=(0, 1))

# Resize the image to reduce computation cost
image_resized = transform.resize(image_rescaled, (1098, 1098, 3), anti_aliasing=True)

# Convert the image to a numpy array
image_processed = np.array(image_resized)

# Load the erosion mask
erosion_mask = gpd.read_file('task_04/masks/Masks_T36UXV_20190427.shp')

# Create a binary label mask based on the erosion mask
label_mask = np.zeros((1098, 1098), dtype=np.uint8)
label_mask[rasterio.features.geometry_mask(erosion_mask.geometry, out_shape=label_mask.shape, transform=image_metadata['transform'], invert=True)] = 1


# Split the data into training and test sets
X_train, X_test, y_train, y_test = train_test_split(
    image_processed,
    label_mask,
    test_size=0.2,
    random_state=42
)

# Define the model architecture
model = keras.Sequential([
    keras.layers.Conv2D(32, (3, 3), activation='relu', input_shape=X_train.shape[1:]),
    keras.layers.MaxPooling2D((2, 2)),
    keras.layers.Conv2D(64, (3, 3), activation='relu'),
    keras.layers.MaxPooling2D((2, 2)),
    keras.layers.Conv2D(128, (3, 3), activation='relu'),
    keras.layers.MaxPooling2D((2, 2)),
    keras.layers.Flatten(),
    keras.layers.Dense(128, activation='relu'),
    keras.layers.Dense(1, activation='sigmoid')
])

# Compile the model
model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])

# Train the model
model.fit(X_train, y_train, epochs=10, validation_split=0.2)

# Evaluate the model on the test set
test_loss, test_acc = model.evaluate(X_test, y_test)

# Print evaluation metrics
print('Test accuracy:', test_acc)
